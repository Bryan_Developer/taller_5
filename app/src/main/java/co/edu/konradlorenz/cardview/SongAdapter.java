package co.edu.konradlorenz.cardview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.MyViewHolder> {


    private Context actualContext;
    private List<Song> songsList;

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView songName, dateSong, durationSong;
        public ImageView songImage;
        private View elementView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.songName = itemView.findViewById(R.id.name_song);
            this.dateSong = itemView.findViewById(R.id.date_song);
            this.durationSong = itemView.findViewById(R.id.duration_song);
            this.songImage = itemView.findViewById(R.id.image_song);
            this.elementView = itemView;
        }
    }

    public SongAdapter(Context actualContext, List<Song> songsList) {
        this.actualContext = actualContext;
        this.songsList = songsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_songs_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final Song song = songsList.get(position);

        holder.elementView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(actualContext,"Click seleccionando: "+ song.getSongName(), Toast.LENGTH_SHORT).show();
            }
        });

        Glide.with(actualContext).load(song.getImageSong()).into(holder.songImage);
        holder.songName.setText(song.getSongName());
        holder.dateSong.setText(song.getDateSong());
        holder.durationSong.setText(song.getDurationSong());
    }

    @Override
    public int getItemCount() {
        return songsList.size();
    }
}
