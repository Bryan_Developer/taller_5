package co.edu.konradlorenz.cardview;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import co.edu.konradlorenz.cardview.R;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AlbumsAdapter adapter;
    private List<Album> albumList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        albumList = new ArrayList<>();
        adapter = new AlbumsAdapter(this, albumList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAlbums();

        try {
            Glide.with(this).load(R.drawable.cover).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Adding few albums for testing
     */
    private void prepareAlbums() {
        int[] covers = new int[]{
                R.drawable.album1,
                R.drawable.album2,
                R.drawable.album3,
                R.drawable.album4,
                R.drawable.album5,
                R.drawable.album6,
                R.drawable.album7,
                R.drawable.album8,
                R.drawable.album9,
                R.drawable.album10,
                R.drawable.album11};


        List<Song> trueRomanceSongsList = new ArrayList<>();

        Song a = new Song(covers[0], getString(R.string.song_a_name), getString(R.string.song_a_date), getString(R.string.song_a_duration));
        trueRomanceSongsList.add(a);
        Song b = new Song(covers[0], getString(R.string.song_b_name), getString(R.string.song_b_date), getString(R.string.song_b_duration));
        trueRomanceSongsList.add(b);
        Song c = new Song(covers[0], getString(R.string.song_c_name), getString(R.string.song_c_date), getString(R.string.song_c_duration));
        trueRomanceSongsList.add(c);
        Song d = new Song(covers[0], getString(R.string.song_d_name), getString(R.string.song_d_date), getString(R.string.song_d_duration));
        trueRomanceSongsList.add(d);
        Song e = new Song(covers[0], getString(R.string.song_e_name), getString(R.string.song_e_date), getString(R.string.song_e_duration));
        trueRomanceSongsList.add(e);

        Album trueRomance = new Album("True Romance", 13, covers[0], trueRomanceSongsList);
        albumList.add(trueRomance);

        //----------------------------------------

        List<Song> xscpaeSongsList = new ArrayList<>();
        Song f = new Song(covers[1], getString(R.string.song_f_name), getString(R.string.song_f_date), getString(R.string.song_f_duration));
        xscpaeSongsList.add(f);
        Song g = new Song(covers[1], getString(R.string.song_g_name), getString(R.string.song_g_date), getString(R.string.song_g_duration));
        xscpaeSongsList.add(g);
        Song h = new Song(covers[1], getString(R.string.song_h_name), getString(R.string.song_h_date), getString(R.string.song_h_duration));
        xscpaeSongsList.add(h);
        Song i = new Song(covers[1], getString(R.string.song_i_name), getString(R.string.song_i_date), getString(R.string.song_i_duration));
        xscpaeSongsList.add(i);
        Song j = new Song(covers[1], getString(R.string.song_j_name), getString(R.string.song_j_date), getString(R.string.song_j_duration));
        xscpaeSongsList.add(j);

        Album xscpae = new Album("Xscpae", 8, covers[1], xscpaeSongsList);
        albumList.add(xscpae);

        //----------------------------------------

        List<Song> maroon5SongsList = new ArrayList<>();

        Song k = new Song(covers[2], getString(R.string.song_k_name), getString(R.string.song_k_date), getString(R.string.song_k_duration));
        maroon5SongsList.add(k);
        Song l = new Song(covers[2], getString(R.string.song_l_name), getString(R.string.song_l_date), getString(R.string.song_l_duration));
        maroon5SongsList.add(l);
        Song m = new Song(covers[2], getString(R.string.song_m_name), getString(R.string.song_m_date), getString(R.string.song_m_duration));
        maroon5SongsList.add(m);
        Song n = new Song(covers[2], getString(R.string.song_n_name), getString(R.string.song_n_date), getString(R.string.song_n_duration));
        maroon5SongsList.add(n);
        Song ñ = new Song(covers[2], getString(R.string.song_ñ_name), getString(R.string.song_ñ_date), getString(R.string.song_ñ_duration));
        maroon5SongsList.add(ñ);

        Album maroon5 = new Album("Maroon 5", 11, covers[2], maroon5SongsList);
        albumList.add(maroon5);

        //----------------------------------------

        List<Song> bornToDieSongsList = new ArrayList<>();

        Song o = new Song(covers[3], getString(R.string.song_o_name), getString(R.string.song_o_date), getString(R.string.song_o_duration));
        bornToDieSongsList.add(o);
        Song p = new Song(covers[3], getString(R.string.song_p_name), getString(R.string.song_p_date), getString(R.string.song_p_duration));
        bornToDieSongsList.add(p);
        Song q = new Song(covers[3], getString(R.string.song_q_name), getString(R.string.song_q_date), getString(R.string.song_q_duration));
        bornToDieSongsList.add(q);
        Song r = new Song(covers[3], getString(R.string.song_r_name), getString(R.string.song_r_date), getString(R.string.song_r_duration));
        bornToDieSongsList.add(r);
        Song s = new Song(covers[3], getString(R.string.song_s_name), getString(R.string.song_s_date), getString(R.string.song_s_duration));
        bornToDieSongsList.add(s);

        Album born_to_die = new Album("Born to Die", 12, covers[3], bornToDieSongsList);
        albumList.add(born_to_die);

        /**
        Album honeymoon = new Album("Honeymoon", 14, covers[4]);
        albumList.add(honeymoon);

        Album i_need_a_doctor = new Album("I Need a Doctor", 1, covers[5]);
        albumList.add(i_need_a_doctor);

        Album loud = new Album("Loud", 11, covers[6]);
        albumList.add(loud);

        Album legend = new Album("Legend", 14, covers[7]);
        albumList.add(legend);

        Album hello = new Album("Hello", 11, covers[8]);
        albumList.add(hello);

        Album greatest_hits = new Album("Greatest Hits", 17, covers[9]);
        albumList.add(greatest_hits);
        **/

        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
