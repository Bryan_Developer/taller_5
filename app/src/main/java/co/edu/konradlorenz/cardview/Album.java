package co.edu.konradlorenz.cardview;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lincoln on 18/05/16.
 */
public class Album implements Serializable {
    private String name;
    private int numOfSongs;
    private int thumbnail;
    private List<Song> songsList;

    public Album() {
    }

    public Album(String name, int numOfSongs, int thumbnail, List<Song> songsList) {
        this.name = name;
        this.numOfSongs = numOfSongs;
        this.thumbnail = thumbnail;
        this.songsList = songsList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfSongs() {
        return numOfSongs;
    }

    public void setNumOfSongs(int numOfSongs) {
        this.numOfSongs = numOfSongs;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<Song> getSongsList() {
        return songsList;
    }

    public void setSongsList(List<Song> songsList) {
        this.songsList = songsList;
    }
}
