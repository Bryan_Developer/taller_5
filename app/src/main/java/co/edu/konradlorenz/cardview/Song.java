package co.edu.konradlorenz.cardview;

import java.io.Serializable;

public class Song implements Serializable {

    private int imageSong;
    private String songName;
    private String dateSong;
    private String durationSong;

    public Song(int imageSong, String songName, String dateSong, String durationSong) {
        this.imageSong = imageSong;
        this.songName = songName;
        this.dateSong = dateSong;
        this.durationSong = durationSong;
    }

    public int getImageSong() {
        return imageSong;
    }

    public void setImageSong(int imageSong) {
        this.imageSong = imageSong;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getDateSong() {
        return dateSong;
    }

    public void setDateSong(String dateSong) {
        this.dateSong = dateSong;
    }

    public String getDurationSong() {
        return durationSong;
    }

    public void setDurationSong(String durationSong) {
        this.durationSong = durationSong;
    }
}
