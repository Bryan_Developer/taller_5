package co.edu.konradlorenz.cardview;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AlbumDetailActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SongAdapter songAdapter;
    private List<Song> songList;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private TextView albumName;
    private ImageView albumImage;
    private Album actualAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_detail);

        findMaterialElements();
        setUpToolbar();
        setUpCollapsingToolbar();
        setUpAppBarLayout();
        setUpLayoutElements();
        initializeArrayAdapter();
        setUpRecyclerView();
        //prepareSongs();
    }

    private void setUpLayoutElements() {
        actualAlbum = (Album) getIntent().getSerializableExtra("album_object");

        albumName.setText(actualAlbum.getName());
        Glide.with(getApplicationContext()).load(actualAlbum.getThumbnail()).into(albumImage);
    }

    private void initializeArrayAdapter() {
        songList = actualAlbum.getSongsList();
        songAdapter = new SongAdapter(this, songList);
    }

    private void setUpRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(songAdapter);
        //recyclerView
    }

    private void setUpAppBarLayout() {
        appBarLayout.setExpanded(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {

            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if(scrollRange == -1){
                    scrollRange = appBarLayout.getTotalScrollRange();
                }else if(scrollRange + verticalOffset == 0){
                    collapsingToolbarLayout.setTitle(getIntent().getStringExtra("album_name"));
                    isShow = true;
                }else if(isShow){
                    collapsingToolbarLayout.setTitle("");
                    isShow = false;
                }
            }
        });
    }

    private void setUpCollapsingToolbar() {
        collapsingToolbarLayout.setTitle("");
    }

    private void setUpToolbar() {
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void findMaterialElements() {
        toolbar = findViewById(R.id.detail_album_toolbar);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_album_detail);
        appBarLayout = findViewById(R.id.app_bar_album_detail);
        recyclerView = findViewById(R.id.recycler_view_album_detail);
        albumName = findViewById(R.id.album_name);
        albumImage = findViewById(R.id.detail_album_image);
    }
}
